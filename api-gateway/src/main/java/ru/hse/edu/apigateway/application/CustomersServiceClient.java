package ru.hse.edu.apigateway.application;

import static java.util.stream.Collectors.joining;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ru.hse.edu.apigateway.dto.OwnerDetails;
import ru.hse.edu.apigateway.dto.Visits;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CustomersServiceClient {

    private static final String HOST = "http://customers-service:8081";

    private final WebClient.Builder webClientBuilder;

    public Mono<OwnerDetails> getOwner(int ownerId) {
        return webClientBuilder.build().get()
            .uri(HOST + "/owners/{ownerId}", ownerId)
            .retrieve()
            .bodyToMono(OwnerDetails.class);
    }

    public Mono<Visits> getVisitsForPets(final List<Integer> petIds) {
        return webClientBuilder.build()
            .get()
            .uri(HOST + "/pets/visits?petId={petId}", joinIds(petIds))
            .retrieve()
            .bodyToMono(Visits.class);
    }

    private String joinIds(List<Integer> petIds) {
        return petIds.stream().map(Object::toString).collect(joining(","));
    }

}
