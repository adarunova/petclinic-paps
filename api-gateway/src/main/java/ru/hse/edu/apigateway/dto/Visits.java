package ru.hse.edu.apigateway.dto;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class Visits {

    List<VisitDetails> items = new ArrayList<>();
}
