package ru.hse.edu.apigateway.web;

import lombok.RequiredArgsConstructor;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreakerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import ru.hse.edu.apigateway.application.CustomersServiceClient;
import ru.hse.edu.apigateway.dto.OwnerDetails;
import ru.hse.edu.apigateway.dto.Visits;

import java.util.function.Function;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/gateway")
public class ApiGatewayController {

    private final CustomersServiceClient customersServiceClient;

    private final ReactiveCircuitBreakerFactory reactiveCircuitBreakerFactory;

    @GetMapping(value = "/owners/{ownerId}")
    public Mono<OwnerDetails> getOwnerDetails(@PathVariable("ownerId") int ownerId) {
        return customersServiceClient.getOwner(ownerId)
            .flatMap(
                owner -> customersServiceClient.getVisitsForPets(owner.getPetIds())
                    .transform(it -> {
                        ReactiveCircuitBreaker cb = reactiveCircuitBreakerFactory.create("getOwnerDetails");
                        return cb.run(it, throwable -> emptyVisitsForPets());
                    })
                    .map(addVisitsToOwner(owner))
            );
    }

    private Function<Visits, OwnerDetails> addVisitsToOwner(OwnerDetails owner) {
        return visits -> {
            owner.getPets().forEach(pet ->
                pet.getVisits().addAll(
                    visits.getItems()
                        .stream()
                        .filter(v -> v.getPetId() == pet.getId())
                        .toList()
                ));
            return owner;
        };
    }

    private Mono<Visits> emptyVisitsForPets() {
        return Mono.just(new Visits());
    }
}
