package ru.hse.edu.customerservice.config;

import org.springframework.context.annotation.Configuration;

import java.util.TimeZone;
import javax.annotation.PostConstruct;

@Configuration
public class LocaleConfig {

    @PostConstruct
    public void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Moscow"));
    }
}
