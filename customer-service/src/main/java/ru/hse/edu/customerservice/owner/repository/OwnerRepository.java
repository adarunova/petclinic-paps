package ru.hse.edu.customerservice.owner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hse.edu.customerservice.owner.model.Owner;

@Repository
public interface OwnerRepository extends JpaRepository<Owner, Integer> {
}
