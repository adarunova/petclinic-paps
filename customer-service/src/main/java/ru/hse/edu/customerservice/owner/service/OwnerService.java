package ru.hse.edu.customerservice.owner.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.customerservice.owner.model.Owner;
import ru.hse.edu.customerservice.owner.repository.OwnerRepository;
import ru.hse.edu.customerservice.web.NotFoundException;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class OwnerService {

    private final OwnerRepository ownerRepository;

    @Transactional
    public Owner create(Owner owner) {
        logger.info("Saving owner {}", owner);
        return ownerRepository.save(owner);
    }

    @Transactional(readOnly = true)
    public Owner findOwner(int ownerId) {
        return ownerRepository.findById(ownerId)
            .orElseThrow(() -> new NotFoundException("Owner %d not found".formatted(ownerId)));
    }

    @Transactional(readOnly = true)
    public List<Owner> findAll() {
        return ownerRepository.findAll();
    }

    @Transactional
    public void updateOwner(int ownerId, Owner ownerRequest) {
        Owner owner = findOwner(ownerId);
        owner.setFirstName(ownerRequest.getFirstName())
            .setLastName(ownerRequest.getLastName())
            .setCity(ownerRequest.getCity())
            .setAddress(ownerRequest.getAddress())
            .setTelephone(ownerRequest.getTelephone());

        logger.info("Updating owner {}", owner);
        ownerRepository.save(owner);
    }
}
