package ru.hse.edu.customerservice.pet.conroller;

import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.hse.edu.customerservice.pet.dto.PetDetails;
import ru.hse.edu.customerservice.pet.dto.PetRequest;
import ru.hse.edu.customerservice.pet.model.Pet;
import ru.hse.edu.customerservice.pet.model.PetType;
import ru.hse.edu.customerservice.pet.service.PetService;
import ru.hse.edu.customerservice.pet.service.PetTypeService;

import java.util.List;

@RestController
@Timed("petclinic.pet")
@RequiredArgsConstructor
class PetController {

    private final PetService petService;
    private final PetTypeService petTypeService;

    @PostMapping("/owners/{ownerId}/pets")
    @ResponseStatus(HttpStatus.CREATED)
    public Pet processCreationForm(@PathVariable("ownerId") int ownerId, @RequestBody PetRequest request) {
        return petService.create(ownerId, request);
    }

    @PutMapping("/owners/*/pets/{petId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void processUpdateForm(@PathVariable("petId") Integer petId, @RequestBody PetRequest request) {
        petService.update(petId, request);
    }

    @GetMapping("/petTypes")
    public List<PetType> getAll() {
        return petTypeService.getAll();
    }

    @GetMapping("owners/*/pets/{petId}")
    public PetDetails findPet(@PathVariable("petId") int petId) {
        return petService.findPetById(petId);
    }

}
