package ru.hse.edu.customerservice.pet.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;
import ru.hse.edu.customerservice.pet.model.PetType;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
public class PetDetails {

    private Integer id;

    private String name;

    private String owner;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    private PetType type;
}
