package ru.hse.edu.customerservice.pet.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.core.style.ToStringCreator;
import ru.hse.edu.customerservice.owner.model.Owner;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Getter
@Setter
@Table(name = "pets")
@Accessors(chain = true)
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Temporal(TemporalType.DATE)
    private Date birthDate;

    private Integer typeId;

    @ManyToOne
    @JoinColumn(name = "typeId", insertable = false, updatable = false)
    private PetType type;

    private Integer ownerId;

    @ManyToOne
    @JoinColumn(name = "ownerId", insertable = false, updatable = false)
    @JsonIgnore
    private Owner owner;

    @Override
    public String toString() {
        return new ToStringCreator(this)
            .append("id", id)
            .append("name", name)
            .append("birthDate", birthDate)
            .append("type", type == null ? null : type.getName())
            .append("ownerFirstname", owner == null ? null : owner.getFirstName())
            .append("ownerLastname", owner == null ? null : owner.getLastName())
            .toString();
    }

}
