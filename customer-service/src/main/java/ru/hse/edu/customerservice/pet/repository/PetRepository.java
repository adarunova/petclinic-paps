package ru.hse.edu.customerservice.pet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.hse.edu.customerservice.pet.model.Pet;

@Repository
public interface PetRepository extends JpaRepository<Pet, Integer> {
}

