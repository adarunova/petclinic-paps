package ru.hse.edu.customerservice.pet.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.customerservice.pet.dto.PetDetails;
import ru.hse.edu.customerservice.pet.dto.PetRequest;
import ru.hse.edu.customerservice.pet.model.Pet;
import ru.hse.edu.customerservice.pet.repository.PetRepository;
import ru.hse.edu.customerservice.web.NotFoundException;

@Service
@RequiredArgsConstructor
@Slf4j
public class PetService {

    private final PetRepository petRepository;

    @Transactional
    public Pet create(int ownerId, PetRequest petRequest) {
        Pet pet = new Pet()
            .setName(petRequest.getName())
            .setBirthDate(petRequest.getBirthDate())
            .setTypeId(petRequest.getTypeId())
            .setOwnerId(ownerId);

        logger.info("Saving pet {}", pet);
        return petRepository.save(pet);
    }

    @Transactional
    public void update(Integer id, PetRequest petRequest) {
        Pet pet = findByIdOrElseThrowNotFound(id);
        pet.setName(petRequest.getName())
            .setBirthDate(petRequest.getBirthDate())
            .setTypeId(petRequest.getTypeId());

        logger.info("Updating pet {}", pet);
        petRepository.save(pet);
    }

    @Transactional(readOnly = true)
    public PetDetails findPetById(int id) {
        Pet pet = findByIdOrElseThrowNotFound(id);

        return new PetDetails()
            .setId(pet.getId())
            .setBirthDate(pet.getBirthDate())
            .setName(pet.getName())
            .setType(pet.getType())
            .setOwner(pet.getOwner().getLastName() + " " + pet.getOwner().getFirstName());
    }

    private Pet findByIdOrElseThrowNotFound(int id) {
        return petRepository.findById(id)
            .orElseThrow(() -> new NotFoundException("Pet %d not found".formatted(id)));
    }
}
