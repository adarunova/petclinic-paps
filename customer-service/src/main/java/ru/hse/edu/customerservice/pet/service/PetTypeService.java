package ru.hse.edu.customerservice.pet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.customerservice.pet.model.PetType;
import ru.hse.edu.customerservice.pet.repository.PetTypeRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PetTypeService {

    private final PetTypeRepository petTypeRepository;

    @Transactional(readOnly = true)
    public List<PetType> getAll() {
        return petTypeRepository.findAll();
    }
}
