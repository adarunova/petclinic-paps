package ru.hse.edu.customerservice.visit.controller;

import io.micrometer.core.annotation.Timed;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.hse.edu.customerservice.visit.dto.VisitsResponse;
import ru.hse.edu.customerservice.visit.model.Visit;
import ru.hse.edu.customerservice.visit.service.VisitService;

import java.util.List;
import javax.validation.Valid;

@RestController
@Timed("petclinic.visit")
@RequiredArgsConstructor
class VisitController {

    private final VisitService visitService;

    @PostMapping("owners/*/pets/{petId}/visits")
    @ResponseStatus(HttpStatus.CREATED)
    public Visit create(@PathVariable("petId") int petId, @Valid @RequestBody Visit visit) {
        return visitService.create(petId, visit);
    }

    @GetMapping("owners/*/pets/{petId}/visits")
    public List<Visit> getByPet(@PathVariable("petId") int petId) {
        return visitService.getByPet(petId);
    }

    @GetMapping("pets/visits")
    public VisitsResponse getByPets(@RequestParam("petId") List<Integer> petIds) {
        return visitService.getByPets(petIds);
    }
}
