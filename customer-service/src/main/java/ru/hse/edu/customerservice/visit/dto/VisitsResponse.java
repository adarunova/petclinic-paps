package ru.hse.edu.customerservice.visit.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.hse.edu.customerservice.visit.model.Visit;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VisitsResponse {

    List<Visit> visits = new ArrayList<>();
}
