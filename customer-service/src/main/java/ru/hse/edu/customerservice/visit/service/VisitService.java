package ru.hse.edu.customerservice.visit.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.customerservice.visit.dto.VisitsResponse;
import ru.hse.edu.customerservice.visit.model.Visit;
import ru.hse.edu.customerservice.visit.repository.VisitRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class VisitService {

    private final VisitRepository visitRepository;

    @Transactional
    public Visit create(int petId, Visit visit) {
        visit.setPetId(petId);
        logger.info("Saving visit {}", visit);
        return visitRepository.save(visit);
    }

    @Transactional(readOnly = true)
    public List<Visit> getByPet(int petId) {
        return visitRepository.findByPetId(petId);
    }

    @Transactional(readOnly = true)
    public VisitsResponse getByPets(List<Integer> petIds) {
        return new VisitsResponse(visitRepository.findByPetIdIn(petIds));
    }
}
