INSERT INTO types VALUES (10001, 'cat');
INSERT INTO types VALUES (10002, 'dog');
INSERT INTO types VALUES (10003, 'lizard');
INSERT INTO types VALUES (10004, 'snake');
INSERT INTO types VALUES (10005, 'bird');
INSERT INTO types VALUES (10006, 'hamster');

INSERT INTO owners VALUES (10001, 'George', 'Franklin', '110 W. Liberty St.', 'Madison', '6085551023');
INSERT INTO owners VALUES (10002, 'Betty', 'Davis', '638 Cardinal Ave.', 'Sun Prairie', '6085551749');
INSERT INTO owners VALUES (10003, 'Eduardo', 'Rodriquez', '2693 Commerce St.', 'McFarland', '6085558763');
INSERT INTO owners VALUES (10004, 'Harold', 'Davis', '563 Friendly St.', 'Windsor', '6085553198');
INSERT INTO owners VALUES (10005, 'Peter', 'McTavish', '2387 S. Fair Way', 'Madison', '6085552765');
INSERT INTO owners VALUES (10006, 'Jean', 'Coleman', '105 N. Lake St.', 'Monona', '6085552654');
INSERT INTO owners VALUES (10007, 'Jeff', 'Black', '1450 Oak Blvd.', 'Monona', '6085555387');
INSERT INTO owners VALUES (10008, 'Maria', 'Escobito', '345 Maple St.', 'Madison', '6085557683');
INSERT INTO owners VALUES (10009, 'David', 'Schroeder', '2749 Blackhawk Trail', 'Madison', '6085559435');
INSERT INTO owners VALUES (10010, 'Carlos', 'Estaban', '2335 Independence La.', 'Waunakee', '6085555487');

INSERT INTO pets VALUES (10001, 'Leo', '2010-09-07', 10001, 10001);
INSERT INTO pets VALUES (10002, 'Basil', '2012-08-06', 10006, 10002);
INSERT INTO pets VALUES (10003, 'Rosy', '2011-04-17', 10002, 10003);
INSERT INTO pets VALUES (10004, 'Jewel', '2010-03-07', 10002, 10003);
INSERT INTO pets VALUES (10005, 'Iggy', '2010-11-30', 10003, 10004);
INSERT INTO pets VALUES (10006, 'George', '2010-01-20', 10004, 10005);
INSERT INTO pets VALUES (10007, 'Samantha', '2012-09-04', 10001, 10006);
INSERT INTO pets VALUES (10008, 'Max', '2012-09-04', 10001, 10006);
INSERT INTO pets VALUES (10009, 'Lucky', '2011-08-06', 10005, 10007);
INSERT INTO pets VALUES (10010, 'Mulligan', '2007-02-24', 10002, 10008);
INSERT INTO pets VALUES (10011, 'Freddy', '2010-03-09', 10005, 10009);
INSERT INTO pets VALUES (10012, 'Lucky', '2010-06-24', 10002, 10010);
INSERT INTO pets VALUES (10013, 'Sly', '2012-06-08', 10001, 10010);

INSERT INTO visits VALUES (10001, 10007, '2013-01-01', 'rabies shot');
INSERT INTO visits VALUES (10002, 10008, '2013-01-02', 'rabies shot');
INSERT INTO visits VALUES (10003, 10008, '2013-01-03', 'neutered');
INSERT INTO visits VALUES (10004, 10007, '2013-01-04', 'spayed');

