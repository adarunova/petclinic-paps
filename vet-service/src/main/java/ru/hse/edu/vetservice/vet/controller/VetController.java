package ru.hse.edu.vetservice.vet.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.hse.edu.vetservice.vet.model.Vet;
import ru.hse.edu.vetservice.vet.service.VetService;

import java.util.List;

@RequestMapping("/vets")
@RestController
@RequiredArgsConstructor
class VetController {

    private final VetService vetService;

    @GetMapping
    public List<Vet> getAll() {
        return vetService.getAll();
    }
}
