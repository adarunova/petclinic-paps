package ru.hse.edu.vetservice.vet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.hse.edu.vetservice.vet.model.Vet;

public interface VetRepository extends JpaRepository<Vet, Integer> {
}
