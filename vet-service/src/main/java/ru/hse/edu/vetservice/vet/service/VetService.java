package ru.hse.edu.vetservice.vet.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.hse.edu.vetservice.vet.model.Vet;
import ru.hse.edu.vetservice.vet.repository.VetRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class VetService {

    private final VetRepository vetRepository;

    @Transactional(readOnly = true)
    public List<Vet> getAll() {
        return vetRepository.findAll();
    }
}
