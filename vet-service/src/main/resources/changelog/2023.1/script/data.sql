INSERT INTO vets VALUES (10001, 'James', 'Carter');
INSERT INTO vets VALUES (10002, 'Helen', 'Leary');
INSERT INTO vets VALUES (10003, 'Linda', 'Douglas');
INSERT INTO vets VALUES (10004, 'Rafael', 'Ortega');
INSERT INTO vets VALUES (10005, 'Henry', 'Stevens');
INSERT INTO vets VALUES (10006, 'Sharon', 'Jenkins');

INSERT INTO specialties VALUES (10001, 'radiology');
INSERT INTO specialties VALUES (10002, 'surgery');
INSERT INTO specialties VALUES (10003, 'dentistry');

INSERT INTO vet_specialties VALUES (10002, 10001);
INSERT INTO vet_specialties VALUES (10003, 10002);
INSERT INTO vet_specialties VALUES (10003, 10003);
INSERT INTO vet_specialties VALUES (10004, 10002);
INSERT INTO vet_specialties VALUES (10005, 10001);
